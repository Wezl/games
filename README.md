# Games

![Open Source](source.png) These are my open-source games (I don't have any closed-source ones)

You can run [tic-80](https://tic80.com/) games (ending in `.tic` file extension) online!

# wyrm.tic

A tic-80 snake-like game with levels, 8 directions, and bullets.

# sandpile.tic

A 2d sandbox game under development.

- A (z on qwerty): build if have gems
- B (x): dig or unbuild
- X (a): swap tile you're on with first inventory slot, swap slot selected in inventory with first slot (when paused)
- Y (s): pause, mess with inventory
- Left/Right: turn (counter)clockwise, select slot in inventory (when paused)
- Up/Down: move forward/backwards

# isoed.tic

An picture editor. Originally, a prototype for a method of movement on a 45-degree rotated grid using the arrow keys.

### controls:

- A: remove tile
- B: add tile
- X: save current position and picture
- Y: load picture from memory
